# arm32v6-postgresql

This is an arm32v6 compatible alpine linux Docker image which runs postgresql.

## Details
- [Source Repository](https://gitlab.com/thebeardedone/arm32v6-postgresql)
- [Dockerfile](https://gitlab.com/thebeardedone/arm32v6-postgresql/blob/master/Dockerfile)

## Running and configuring the postgres server
```bash
docker run -d --name postgres \
  -p 5432:5432 \
  -v <postgres_data_path>:/var/lib/postgresql/data \
  -e POSTGRES_DB='postgres' \
  -e POSTGRES_USER='postgres' \
  -e POSTGRES_PASSWORD='Test1234' \
  thebeardedone/arm32v6-postgresql
```

## Quick start

### Create image

Checkout this repository and run `make build`.
```bash
git clone https://gitlab.com/thebeardedone/arm32v6-postgresql
cd arm32v6-postgresql
make build
```

Copy `env.template` to `env`. Modify the environment variables as necessary.
The following lines need to be changed.
```bash
PG_DATA=<postgres_data_path>
```

### Run container
```bash
make run
```
